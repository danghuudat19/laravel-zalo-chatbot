<?php


return [
    'zalo_app_id' => env('ZALO_APP_ID'),
    'app_secret' => env('APP_SECRET'),
    'callback_url' => env('CALLBACK_URL'),
    'access_token' => env('ACCESS_TOKEN'),
    'refresh_token' => env('REFRESH_TOKEN'),
    'code_challenge' => env('CODE_CHALLENGE'),
];
