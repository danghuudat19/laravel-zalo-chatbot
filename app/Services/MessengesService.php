<?php


namespace App\Services;


use Illuminate\Support\Facades\Http;

class MessengesService
{
    public function relyMsg($sender_id){
        return Http::withHeaders([
            'access_token' => config('zalo.access_token'),
        ])->post('https://openapi.zalo.me/v2.0/oa/message', [
            'recipient' => [
                'user_id' => $sender_id,
            ],
            'message' => [
                'text' => 'test_id',
            ]
        ]);
    }
}
