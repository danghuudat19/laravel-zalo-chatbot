<?php

namespace App\Http\Controllers;

use App\Services\MessengesService;
use Illuminate\Http\Request;

class MessengesController extends Controller
{
    protected $messengesService;

    public function __construct(
        MessengesService $messengesService
    )
    {
        $this->messengesService = $messengesService;
    }

    public function relyMsg(Request $request){
        $sender_id = $request->sender['id'];
        $response = $this->messengesService->relyMsg($sender_id);
        return response($response);
    }

    public function getZalo(Request $request){
        dd($request);
    }

}
